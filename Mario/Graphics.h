#pragma once
#include <memory>
#include <map>
#include <string>
#include "D3D.h"
#include "ColorShader.h"
#include "Model.h"
#include "DynamicModel.h"
#include "Camera.h"


//class for handlig DX11 graphics components
__declspec(align(16)) class Graphics
{
public:
	Graphics();
	~Graphics();
	void* operator new(size_t i);
	void operator delete(void* p);

	void beginScene();
	void endScene();
	void Render(std::string modelName, const DirectX::XMMATRIX& worldMatrix, 
		int indexOffset=0, int vertexOffset=0, UINT indexCount=0);
	void Render(std::string modelName, int indexOffset = 0, 
		int vertexOffset = 0, UINT indexCount = 0);

	void Init(HWND hWnd);     // initialize D3D, throws GameError on failure 
	void Clean();         // close D3D and release memory

	void setCameraPos(float x, float y, float z);

	std::shared_ptr<Model> addModel(std::string name, std::vector<Model::Vertex> &vertices,
		std::vector<UINT> &indices);
	std::shared_ptr<DynamicModel> addDynamicModel(std::string name,int vertices,
		std::vector<UINT> &indices);

private:
	std::unique_ptr <D3DClass> mD3D;
	std::unique_ptr <ColorShader> mFX;
	std::map<std::string, std::shared_ptr <Model>> mModels;
	std::unique_ptr <Camera> mCamera;

	DirectX::XMMATRIX mViewMatrix;
	DirectX::XMMATRIX mProjectionMatrix;
};
