#include "GameTimer.h"



GameTimer::GameTimer()
	: mSecondsPerCount(0.0), mDeltaTime(-1.0), mBaseTime(0),
	mPausedTime(0), mPrevTime(0), mCurrTime(0), mStopped(false)
{
	__int64 countsPerSec;
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
	mSecondsPerCount = 1.0 / (double)countsPerSec;
}

GameTimer::~GameTimer()
{
}

float GameTimer::TotalTime() const
{
	if (mStopped)
	{
		return (float)(((mStopTime - mPausedTime) - mBaseTime)*mSecondsPerCount);
	}
	else
	{
		return (float)(((mCurrTime - mPausedTime) - mBaseTime)*mSecondsPerCount);
	}
}


void GameTimer::Tick()
{
	if (mStopped)
	{
		mDeltaTime = 0.0;
		return;
	}
	// Pobierz czas zegara dla tej klatki.
	__int64 currTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
	mCurrTime = currTime;
	// R�nica czasowa mi�dzy t� klatk� a poprzedni�.
	mDeltaTime = (mCurrTime - mPrevTime)*mSecondsPerCount;
	// Zapisz warto�� czasu. Zostanie ona wykorzystana przy przetwarzaniu nast�pnej klatki.
	mPrevTime = mCurrTime;
	// Wymu� warto�� nieujemn�. Dokumentacja klasy CDXUTTimer nadmienia, �e je�li
	// procesor przejdzie w tryb oszcz�dzania energii lub gdy zostaniemy prze��czeni do innego
	// procesora, warto�� mDeltaTime mo�e by� ujemna.
	if (mDeltaTime < 0.0)
	{
		mDeltaTime = 0.0;
	}
}

void GameTimer::Reset()
{
	__int64 currTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
	mBaseTime = currTime;
	mPrevTime = currTime;
	mStopTime = 0;
	mStopped = false;
}

void GameTimer::Start()
{
	__int64 startTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&startTime);
	// Zsumuj czas pomi�dzy parami stop-start.
	// Je�eli wychodzimy z trybu pauzy...
	if (mStopped)
	{
		// aktualizuj skumulowany czas w trybie pauzy
		mPausedTime += (startTime - mStopTime);
		// poniewa� wznawiamy czas, aktualny
		// czas dla poprzedniej klatki nie jest poprawny, gdy� aplikacja by�a w trybie pauzy.
		// Resetujemy go zatem do bie��cego czasu.
		mPrevTime = startTime;
		// kontynuacja dzia�ania�
		mStopTime = 0;
		mStopped = false;
	}
}

void GameTimer::Stop()
{
	// Je�li gra jest ju� zatrzymana, nie r�b nic.
	if (!mStopped)
	{
		__int64 currTime;
		QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
		// W przeciwnym razie zapisz moment zatrzymania i ustaw
		// warto�� zmiennej logicznej, informuj�c� o zatrzymaniu czasu.
		mStopTime = currTime;
		mStopped = true;
	}
}
