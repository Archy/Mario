#include "Graphics.h"
#include <memory>
#include <d3dcompiler.h>
#include "Constants.h"
#include "GameError.h"
#include <iostream>


Graphics::Graphics()
{
}


Graphics::~Graphics()
{
}

void * Graphics::operator new(size_t i)
{
	return _mm_malloc(i,16);
}

void Graphics::operator delete(void * p)
{
	_mm_free(p);
}

void Graphics::beginScene()
{
	mD3D->BeginScene();

	// Generate the view matrix based on the camera's position.
	mCamera->Render();
	mCamera->GetViewMatrix(mViewMatrix);
	mD3D->GetProjectionMatrix(mProjectionMatrix);
}

void Graphics::endScene()
{
	mD3D->EndScene();
}

void Graphics::Render(std::string modelName, const DirectX::XMMATRIX& worldMatrix, 
	int indexOffset, int vertexOffset, UINT indexCount)
{
	auto it = mModels.find(modelName);
	if (it == mModels.end())
	{
		std::cout << "No such model: " << modelName << "\n";
		//TODO throw exception
		return;
	}

	//set nr of of indexes
	indexCount = (indexCount == 0 ? mModels[modelName]->GetIndexCount() : indexCount);

	// Put the model vertex and index buffers on the graphics pipeline to prepare them for drawing.	
	mModels[modelName]->Render(mD3D->GetDeviceContext());
	// Render the model using the color shader.
	try
	{
		mFX->Render(mD3D->GetDeviceContext(), indexCount,
			indexOffset, vertexOffset, worldMatrix, mViewMatrix, mProjectionMatrix);
	}
	catch (const GameError& e)
	{
		std::cout << "coudnt render model: " << modelName << "\n" << e.getMessage();
	}
}

void Graphics::Render(std::string modelName, int indexOffset, int vertexOffset, 
	UINT indexCount)
{
	auto it = mModels.find(modelName);
	if (it == mModels.end())
	{
		std::cout << "No such model: " << modelName << "\n";
		//TODO throw exception
		return;
	}

	//set nr of of indexes
	indexCount = (indexCount == 0 ? mModels[modelName]->GetIndexCount() : indexCount);

	DirectX::XMMATRIX worldMatrix;
	// Get the world, view, and projection matrices from the camera and d3d objects.
	mD3D->GetWorldMatrix(worldMatrix);


	// Put the model vertex and index buffers on the graphics pipeline to prepare them for drawing.	
	mModels[modelName]->Render(mD3D->GetDeviceContext());
	// Render the model using the color shader.
	try
	{
		mFX->Render(mD3D->GetDeviceContext(), indexCount,
			indexOffset, vertexOffset, worldMatrix, mViewMatrix, mProjectionMatrix);
	}
	catch (const GameError& e)
	{
		std::cout << "coudnt render model: " << modelName << "\n" << e.getMessage();
	}

}


void Graphics::Init(HWND hWnd)
{
	//DirectX init
	mD3D = std::make_unique<D3DClass>();
	mD3D->InitD3D(hWnd);

	mFX = std::make_unique<ColorShader>();
	mFX->Initialize(mD3D->GetDevice());

	mCamera = std::make_unique<Camera>();
}

void Graphics::Clean(void)
{
	//	NEVER CLOSED COM OBJECT WILL KEEP RUNNING IN THE BACKGROUND 
	//	UNTIL COMPUTER NEXT REBOOT

	// close and release all existing COM objects
	for (auto &it : mModels) 
	{
		it.second->clean();
	}
	
	mFX->Clean();
	mD3D->CleanD3D();
}

void Graphics::setCameraPos(float x, float y, float z)
{
	mCamera->SetPosition(x, y, z);
}

std::shared_ptr<Model> Graphics::addModel(std::string name, 
	std::vector<Model::Vertex> &vertices, std::vector<UINT> &indices)
{
	auto pos = mModels.find(name);
	if (pos != mModels.end())
	{
		//TODO throw exception
		return nullptr;
	}

	std::shared_ptr<Model> tempModel = std::make_shared<Model>();
	tempModel->Initialize(mD3D->GetDevice(), vertices, indices);
	mModels.emplace(name, tempModel);

	return tempModel;
}

std::shared_ptr<DynamicModel> Graphics::addDynamicModel(std::string name, int vertices, std::vector<UINT>& indices)
{
	auto pos = mModels.find(name);
	if (pos != mModels.end())
	{
		//TODO throw exception
		return nullptr;
	}

	std::shared_ptr<DynamicModel> tempModel = std::make_shared<DynamicModel>();
	tempModel->Initialize(mD3D->GetDevice(), mD3D->GetDeviceContext(), vertices, indices);
	mModels.emplace(name, tempModel);

	return tempModel;
}
