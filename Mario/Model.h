#pragma once
#include <d3d11.h>
#include <directxmath.h>
#include <vector>
#include "Constants.h"
#include "GameError.h"

class Model
{
public:
	struct Vertex
	{
		DirectX::XMFLOAT3 Pos;
		DirectX::XMFLOAT4 Color;
	};

public:
	Model();
	~Model();

	virtual void Initialize(ID3D11Device* dev, std::vector<Vertex> &vertices, std::vector<UINT> &indices);
	void clean();

	void Render(ID3D11DeviceContext* devCon);

	int GetIndexCount() const { return mIndexCount; }

protected:
	ID3D11Buffer* mVB;
	ID3D11Buffer* mIB;
	int mVertexCount, mIndexCount;
};

