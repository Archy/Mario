#pragma once
#include "Model.h"
class DynamicModel :
	public Model
{
public:
	DynamicModel();
	~DynamicModel();

	virtual void Initialize(ID3D11Device* dev, ID3D11DeviceContext * devCon, int verticesSize, std::vector<UINT> &indices);
	void updateBuffer(std::vector<Vertex>& vertices);

private:
	ID3D11DeviceContext * mDevCon;
};

