#pragma once
#include <memory>
#include "DynamicModel.h"
#include "Graphics.h"

class Waves
{
public:
	Waves();
	~Waves();

	void init(UINT m, UINT n, float dx, float dt, float speed, float damping);
	void createModel(std::unique_ptr<Graphics> &graphic);
	
	void Update(float dt);
	
	
	void render(std::unique_ptr<Graphics>& graphic);

private:
	void Disturb(UINT i, UINT j, float magnitude);
	float RandF(float a, float b);

private:
	UINT mNumRows;
	UINT mNumCols;

	UINT mVertexCount;
	UINT mTriangleCount;

	// Simulation constants we can precompute.
	float mK1;
	float mK2;
	float mK3;

	float mTimeStep;
	float mSpatialStep;

	DirectX::XMFLOAT3* mPrevSolution;
	DirectX::XMFLOAT3* mCurrSolution;

	//not needed
	//DirectX::XMFLOAT4X4 mWavesWorld;

	std::shared_ptr <DynamicModel> mModel;
	std::string name;
};

