#include "ColorShader.h"



ColorShader::ColorShader() : 
	mFX(nullptr), mTech(nullptr), mfxWorldViewProj(nullptr),
	mLayout(nullptr), mMatrixBuffer(nullptr)
{
}


ColorShader::~ColorShader()
{
}

void ColorShader::Initialize(ID3D11Device * device)
{
	InitializeShader(device);
	createVertexLayout(device);
}

void ColorShader::Clean()
{
	COMSF(mLayout);
	COMSF(mMatrixBuffer);

	COMSF(mFX);
	COMSF(mTech);
	COMSF(mfxWorldViewProj);
}

void ColorShader::Render(ID3D11DeviceContext* deviceContext, 
	int indexCount, int indexOffset, int vertexOffset, 
	const DirectX::XMMATRIX &worldMatrix, const DirectX::XMMATRIX &viewMatrix,
	const DirectX::XMMATRIX &projectionMatrix)
{
	bool result;
	// Set the shader parameters that it will use for rendering.
	result = SetShaderParameters(deviceContext, worldMatrix, viewMatrix, projectionMatrix);
	if (!result)
	{
		//throw some exception
	}

	// Now render the prepared buffers with the shader.
	RenderShader(deviceContext, indexCount, indexOffset, vertexOffset);
}


void ColorShader::InitializeShader(ID3D11Device * device)
{
	HRESULT result;
	DWORD shaderFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
	shaderFlags |= D3D10_SHADER_DEBUG;
	shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif
	ID3D10Blob* compiledShader = 0;
	ID3D10Blob* compilationMsgs = 0;

	result = D3DCompileFromFile(L"shader.fx", 0,
		0, 0, "fx_5_0", shaderFlags,
		0, &compiledShader, &compilationMsgs);

	// compilationMsgs przechowuje komunikaty o b��dach lub ostrze�enia.
	if (compilationMsgs != 0)
	{
		MessageBoxA(0, (char*)compilationMsgs->GetBufferPointer(), 0, 0);
		compilationMsgs->Release();
	}
	// Nawet je�li compilationMsgs jest pusta, upewnij si�, �e
	// nie by�o innych b��d�w.
	if (!SUCCEEDED(result))
	{
		throw GameError(FATAL_ERROR, "Error compiling shader");
	}


	//create effect
	result = D3DX11CreateEffectFromMemory(
		compiledShader->GetBufferPointer(),
		compiledShader->GetBufferSize(),
		0, device, &mFX);

	// Koniec pracy ze skompilowanym shaderem.
	compiledShader->Release();

	if (FAILED(result))
	{
		throw GameError(FATAL_ERROR, "Error creating effect");
	}

	mTech = mFX->GetTechniqueByName("ColorTech");
	mfxWorldViewProj = mFX->GetVariableByName("gWorldViewProj")->AsMatrix();
}

void ColorShader::createVertexLayout(ID3D11Device * device)
{
	HRESULT result;
	D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12,
		D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	D3DX11_PASS_DESC passDesc;
	mTech->GetPassByIndex(0)->GetDesc(&passDesc);
	result = device->CreateInputLayout(vertexDesc, 2, passDesc.pIAInputSignature,
		passDesc.IAInputSignatureSize, &mLayout);
	if (FAILED(result)) {
		throw GameError(FATAL_ERROR, "Error creating input layout");
	}
}

bool ColorShader::SetShaderParameters(ID3D11DeviceContext *deviceContext, 
	const DirectX::XMMATRIX &worldMatrix, const DirectX::XMMATRIX &viewMatrix, 
	const DirectX::XMMATRIX &projectionMatrix)
{

	// Set the vertex input layout.
	deviceContext->IASetInputLayout(mLayout);

	DirectX::XMMATRIX worldViewProj;
	worldViewProj = worldMatrix*viewMatrix*projectionMatrix;
	mfxWorldViewProj->SetMatrix(reinterpret_cast<float*>(&worldViewProj));
	
	return true;
}

void ColorShader::RenderShader(ID3D11DeviceContext* deviceContext, 
	int indexCount, int indexOffset, int vertexOffset)
{
	D3DX11_TECHNIQUE_DESC techDesc;
	mTech->GetDesc(&techDesc);
	for (UINT p = 0; p < techDesc.Passes; ++p)
	{
		mTech->GetPassByIndex(p)->Apply(0, deviceContext);
		deviceContext->DrawIndexed(indexCount, indexOffset, vertexOffset);
	}
}
