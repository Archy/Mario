#pragma once
#include <Windows.h>

class GameTimer
{
public:
	GameTimer();
	~GameTimer();

	float TotalTime()const; // w sekundach
	float DeltaTime()const { return (float)mDeltaTime; }; // w sekundach
	
	void Reset(); // Wywo�aj przed p�tl� komunikat�w.
	void Start(); // Wywo�aj, gdy aplikacja jest niezatrzymana.
	void Stop(); // Wywo�aj, gdy aplikacja jest zatrzymana.
	void Tick(); // Wywo�uj przy ka�dej klatce.

private:
	double mSecondsPerCount;
	double mDeltaTime;
	
	__int64 mBaseTime;
	__int64 mPausedTime;
	__int64 mStopTime;
	__int64 mPrevTime;
	__int64 mCurrTime;
	
	bool mStopped;
};

