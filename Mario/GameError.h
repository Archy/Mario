#pragma once
#include <string>
#include <exception>

// Error codes
// Negative numbers are fatal errors that may require the game to be
// shutdown.
// Positive numbers are warnings that do not require the game to be
// shutdown.
enum GameErrorsList
{
	D3D_INIT_ERROR = -2,
	FATAL_ERROR = -1,
	WARNING = 1
};

// Game Error class. Thrown when an error is detected by the game engine.
// Inherits from std::exception
class GameError : public std::exception
{
public:
	// Default constructor
	GameError() throw() :
		m_errorCode(GameErrorsList::FATAL_ERROR), m_message("Undefined Error in game.")
	{}
	// Copy constructor
	GameError(const GameError& e) throw() : 
		std::exception(e), m_errorCode(e.m_errorCode), m_message(e.m_message) 
	{}
	// Constructor with args
	GameError(GameErrorsList code, const std::string &s) throw() :
		m_errorCode(code), m_message(s)
	{}
	
	// Assignment operator
	GameError& operator= (const GameError& rhs) throw()
	{
		std::exception::operator=(rhs);
		this->m_errorCode = rhs.m_errorCode;
		this->m_message = rhs.m_message;
	}

	// Destructor
	virtual ~GameError() throw() {};

	// Override what from base class
	virtual const char* what() const throw() { return this->getMessage(); }
	const char* getMessage() const throw() { return m_message.c_str(); }
	int getErrorCode() const throw() { return m_errorCode; }

private:
	GameErrorsList m_errorCode;
	std::string m_message;
};

