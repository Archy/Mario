#include "D3D.h"
#include "Constants.h"
#include "GameError.h"


D3DClass::D3DClass() : SCREEN_DEPTH(1000.0), SCREEN_NEAR(1.0),
	m_swapchain(nullptr), m_dev(nullptr), m_devcon(nullptr), m_backbuffer(nullptr),
	mEnable4xMsaa(true), mVsync(false)
{
}

D3DClass::~D3DClass()
{
}

void * D3DClass::operator new(size_t i)
{
	return _mm_malloc(i,16);
}

void D3DClass::operator delete(void * p)
{
	_mm_free(p);
}

void D3DClass::InitD3D(HWND hWnd)
{
	//Create D3Device
	this->createDevice(hWnd);
	//Create SwapChain
	this->createSwapchain(hWnd);
	//Setting the Render Target
	this->setRenderTarget();
	//create depth/stencil buffer
	this->createDepthStencilBuffer();
	// set the render target as the back buffer
	this->m_devcon->OMSetRenderTargets(1, &m_backbuffer, NULL);
	// Set the viewport
	this->setViewpoint();
	//create Rasterizer State
	createRasterizerState();

	setMatrices();
}

void D3DClass::CleanD3D(void)
{
	m_swapchain->Release();
	mDepthStencilView->Release();
	mDepthStencilBuffer->Release();
	m_backbuffer->Release();
	m_dev->Release();
	m_devcon->Release();
	COMSF(mRasterState);
}

void D3DClass::BeginScene()
{
	// Clear the back buffer.
	m_devcon->ClearRenderTargetView(m_backbuffer, reinterpret_cast<const float*>(&Colors::LightSteelBlue));
	// Clear the depth buffer.
	m_devcon->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	m_devcon->RSSetState(mRasterState);
}

void D3DClass::EndScene()
{
	// Present the back buffer to the screen since rendering is complete.
	if (mVsync)
	{
		// Lock to screen refresh rate.
		m_swapchain->Present(1, 0);
	}
	else
	{
		// Present as fast as possible.
		m_swapchain->Present(0, 0);
	}
}

void D3DClass::GetProjectionMatrix(DirectX::XMMATRIX & projectionMatrix)
{
	projectionMatrix = m_projectionMatrix;
}

void D3DClass::GetWorldMatrix(DirectX::XMMATRIX & worldMatrix)
{
	worldMatrix = m_worldMatrix;
}

void D3DClass::GetOrthoMatrix(DirectX::XMMATRIX & orthoMatrix)
{
	orthoMatrix = m_orthoMatrix;
}

void D3DClass::createDevice(HWND hWnd)
{
	HRESULT result;
	bool success = false;

	//loop and test every driver type with every feature level
	// Driver types
	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,	//creates Direct3D 10.1 Windows Advanced Rasterization Platform
		D3D_DRIVER_TYPE_REFERENCE,
	};
	// feature levels
	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	int numDriverTypes = ARRAYSIZE(driverTypes);
	int numFeatureLevels = ARRAYSIZE(featureLevels);

	UINT createDeviceFlags = 0; //D3D11_CREATE_DEVICE_BGRA_SUPPORT???
#if defined(DEBUG) || defined(_DEBUG)
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	for (int driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		// create a device, device context and swap chain using the information in the scd struct
		result = D3D11CreateDevice(
			NULL,	//default card
			driverTypes[driverTypeIndex],
			NULL,
			createDeviceFlags,
			featureLevels,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&this->m_dev,
			&this->m_featureLevel,
			&this->m_devcon);

		if (SUCCEEDED(result))
		{
			success = true;
			break;
		}

	}
	if (!success)
	{
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Direct3D device");
	}
#ifdef DEBUG
	if (m_featureLevel != D3D_FEATURE_LEVEL_11_0) {
		result = D3D11CreateDevice(
			NULL,	//default card
			D3D_DRIVER_TYPE_REFERENCE,
			NULL,
			D3D11_CREATE_DEVICE_DEBUG,
			NULL,
			NULL,
			D3D11_SDK_VERSION,
			&this->m_dev,
			&this->m_featureLevel,
			&this->m_devcon);
		if (FAILED(result)) {
			MessageBox(NULL, "Wyrzu� komputer i kup nowy", "DUPA", MB_OK);
			throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Direct3D device");
		}
		else {
			MessageBox(NULL, "Moze bedzie dzia�a�. Ae i tak wyrzu� komputer i kup nowy", "DUPA", MB_OK);
		}
	}
#endif // DEBUG
}

void D3DClass::createSwapchain(HWND hWnd)
{
	HRESULT result;
	DXGI_SWAP_CHAIN_DESC sd;
	UINT m4xMsaaQuality;
	
	result = m_dev->CheckMultisampleQualityLevels(
		DXGI_FORMAT_R8G8B8A8_UNORM, 4, &m4xMsaaQuality);
#ifndef DEBUG
	assert(m4xMsaaQuality > 0);
#endif // !DEBUG

	// create a struct to hold information about the swap chain
	// fill the swap chain description struct
	sd.BufferDesc.Width = GAME_WIDTH; // u�yj wymiar�w obszaru klienta okna
	sd.BufferDesc.Height = GAME_HEIGHT;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	// U�y� antyaliasingu z 4-krotnym pr�bkowaniem?
	if (mEnable4xMsaa)
	{
		sd.SampleDesc.Count = 4;
		// Warto�� m4xMsaaQuality uzyskujemy przez wywo�anie CheckMultisampleQualityLevels().
		sd.SampleDesc.Quality = m4xMsaaQuality - 1;
	}
	// Brak antyaliasingu z 4-krotnym pr�bkowaniem
	else
	{
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
	}
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.BufferCount = 1;
	sd.OutputWindow = hWnd;
	sd.Windowed = true;
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	sd.Flags = 0;

	//get IDXGIFactory interface
	IDXGIDevice* dxgiDevice = 0;
	result = m_dev->QueryInterface(__uuidof(IDXGIDevice), (void**)&dxgiDevice);
	if (FAILED(result)) {
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Direct3D swapchain");
	}
	IDXGIAdapter* dxgiAdapter = 0;
	result = dxgiDevice->GetParent(__uuidof(IDXGIAdapter), (void**)&dxgiAdapter);
	if (FAILED(result)) {
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Direct3D swapchain");
	}

	IDXGIFactory* dxgiFactory = 0;
	result = dxgiAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&dxgiFactory);
	if (FAILED(result)) {
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Direct3D swapchain");
	}
	// create swapchain
	result = dxgiFactory->CreateSwapChain(m_dev, &sd, &m_swapchain);
	if (FAILED(result)) {
		// release unsed com interfaces
		dxgiDevice->Release();
		dxgiAdapter->Release();
		dxgiFactory->Release();
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Direct3D swapchain");
	}
	// release unsed com interfaces
	dxgiDevice->Release();
	dxgiAdapter->Release();
	dxgiFactory->Release();

}

void D3DClass::createDepthStencilBuffer()
{
	UINT m4xMsaaQuality;
	HRESULT result;
	D3D11_TEXTURE2D_DESC depthStencilDesc;
	

	result = m_dev->CheckMultisampleQualityLevels(
		DXGI_FORMAT_R8G8B8A8_UNORM, 4, &m4xMsaaQuality);

	depthStencilDesc.Width = GAME_WIDTH;
	depthStencilDesc.Height = GAME_HEIGHT;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	// U�y� antyaliasingu z 4-krotnym pr�bkowaniem?
	if (mEnable4xMsaa)
	{
		depthStencilDesc.SampleDesc.Count = 4;
		depthStencilDesc.SampleDesc.Quality = m4xMsaaQuality - 1;
	}
	// Brak antyaliasingu z 4-krotnym pr�bkowaniem.
	else
	{
		depthStencilDesc.SampleDesc.Count = 1;
		depthStencilDesc.SampleDesc.Quality = 0;
	}
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;
	
	result = m_dev->CreateTexture2D(
		&depthStencilDesc, // Opis tworzonej tekstury.
		0,
		&mDepthStencilBuffer); // Zwr�� wska�nik do bufora g��boko�ci/szablonu.
	if (!SUCCEEDED(result))
	{
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating depth/stencil bufer");
	}
	result = m_dev->CreateDepthStencilView(
		mDepthStencilBuffer, // Zas�b, dla kt�rego tworzymy widok.
		0,
		&mDepthStencilView); // Zwr�� widok g��boko�ci/szablonu.
	if (!SUCCEEDED(result))
	{
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating depth/stencil bufer");
	}
}

void D3DClass::setViewpoint()
{
	D3D11_VIEWPORT viewport;
	ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.Width = static_cast<float>(GAME_WIDTH);
	viewport.Height = static_cast<float>(GAME_HEIGHT);
	viewport.MinDepth = 0.0;
	//TODO set to 0.0 for 2D aplication
	viewport.MaxDepth = 1.0;

	this->m_devcon->RSSetViewports(1, &viewport);
}

void D3DClass::setRenderTarget()
{
	// get the address of the back buffer
	ID3D11Texture2D *pBackBuffer;
	HRESULT result;

	result = this->m_swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
	if (!SUCCEEDED(result))
	{
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error getting backbufer");
	}

	// use the back buffer address to create the render target
	this->m_dev->CreateRenderTargetView(pBackBuffer, NULL, &m_backbuffer);
	if (!SUCCEEDED(result))
	{
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating render target");
	}
	pBackBuffer->Release();
}

void D3DClass::setMatrices()
{
	float fieldOfView = 3.141592654f / 4.0f;
	float screenAspect = (float)GAME_WIDTH / (float)GAME_HEIGHT;

	// Create the projection matrix for 3D rendering.
	m_projectionMatrix = DirectX::XMMatrixPerspectiveFovLH(
		fieldOfView,	// pionowy k�t pola widzenia w radianach
		screenAspect,	// proporcje obrazu = szeroko��/wysoko��
		SCREEN_NEAR,		// odleg�o�� bli�szej p�aszczyzny
		SCREEN_DEPTH);	// odleg�o�� dalszej p�aszczyzny

	// Initialize the world matrix to the identity matrix.
	m_worldMatrix = DirectX::XMMatrixIdentity();

	// Create an orthographic projection matrix for 2D rendering.
	m_orthoMatrix = DirectX::XMMatrixOrthographicLH((float)GAME_WIDTH,
				(float)GAME_HEIGHT, SCREEN_NEAR, SCREEN_DEPTH);
}

void D3DClass::createRasterizerState()
{
	HRESULT result;

	D3D11_RASTERIZER_DESC wireframeDesc;
	ZeroMemory(&wireframeDesc, sizeof(D3D11_RASTERIZER_DESC));
	wireframeDesc.FillMode = D3D11_FILL_WIREFRAME;
	wireframeDesc.CullMode = D3D11_CULL_BACK;
	wireframeDesc.FrontCounterClockwise = false;
	wireframeDesc.DepthClipEnable = true;

	result = m_dev->CreateRasterizerState(&wireframeDesc, &mRasterState);

	if (!SUCCEEDED(result))
	{
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Rasterizer State");
	}
}
