#pragma once
#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include "d3dx11Effect.h"

#include "Constants.h"
#include "GameError.h"

class ColorShader
{
public:
	ColorShader();
	~ColorShader();

	//initialize shaders. Throws exception on error
	void Initialize(ID3D11Device* device);
	void Clean();

	//sets the shader parameters and then draws the prepared model vertices.
	//Throws expetion on error
	void Render(ID3D11DeviceContext* deviceContext, 
		int indexCount, int indexOffset, int vertexOffset,
		const DirectX::XMMATRIX &worldMatrix, const DirectX::XMMATRIX &viewMatrix,
		const DirectX::XMMATRIX &projectionMatrix);


private:
	void InitializeShader(ID3D11Device * device);
	void createVertexLayout(ID3D11Device * device);

	bool SetShaderParameters(ID3D11DeviceContext*, const DirectX::XMMATRIX&, 
		const DirectX::XMMATRIX&, const DirectX::XMMATRIX&);
	void RenderShader(ID3D11DeviceContext*, int, int, int);

private:
	ID3D11InputLayout* mLayout;
	ID3D11Buffer* mMatrixBuffer;

	ID3DX11Effect* mFX;
	ID3DX11EffectTechnique* mTech;
	ID3DX11EffectMatrixVariable* mfxWorldViewProj;
};

