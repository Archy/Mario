#pragma once
#include "Game.h"
#include "Waves.h"

class Box : public Game
{
public:
	Box();
	~Box();

	virtual void Initialize(HWND hwnd);

protected:
	virtual void Update();
	virtual void Render();

private:

//################################################
	//box app
	void createBox();

//################################################
	//hills app
	void createHills();
	float GetHeight(float x, float z)const;

//################################################
	//shapes app
	void createShapes();
	void initSapes();
	void drawShapes();
	DirectX::XMFLOAT4X4 mSphereWorld[10];
	DirectX::XMFLOAT4X4 mCylWorld[10];
	DirectX::XMFLOAT4X4 mBoxWorld;
	DirectX::XMFLOAT4X4 mGridWorld;
	DirectX::XMFLOAT4X4 mCenterSphere;

	int mBoxVertexOffset;
	int mGridVertexOffset;
	int mSphereVertexOffset;
	int mCylinderVertexOffset;

	UINT mBoxIndexOffset;
	UINT mGridIndexOffset;
	UINT mSphereIndexOffset;
	UINT mCylinderIndexOffset;

	UINT mBoxIndexCount;
	UINT mGridIndexCount;
	UINT mSphereIndexCount;
	UINT mCylinderIndexCount;

//################################################
//skull
	void loadModel(std::string path, std::string name);
	DirectX::XMFLOAT4X4 mSkullWorld;

//################################################
//waves
	void createLand();

	Waves mWaves;

private:
	POINT mMouse;

	float mTheta;
	float mPhi;
	float mRadius;
};

