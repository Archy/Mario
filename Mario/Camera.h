#pragma once
#include <directxmath.h>

__declspec(align(16)) class Camera
{
public:
	Camera();
	~Camera();

	void* operator new(size_t i);
	void operator delete(void* p);

	void SetPosition(float x, float y, float z);
	void SetRotation(float x, float y, float z);

	void Render();
	void GetViewMatrix(DirectX::XMMATRIX& viewMatrix) { viewMatrix = mViewMatrix; }

private:
	float mPositionX, mPositionY, mPositionZ;
	float mRotationX, mRotationY, mRotationZ;
	DirectX::XMMATRIX mViewMatrix;
};

